package com.example.harrypotter.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.harrypotter.adapter.HPAdapter
import com.example.harrypotter.databinding.FragmentHogwatsStaffBinding
import com.example.harrypotter.viewModel.StaffViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class HogwartsStaffFragment : Fragment() {

    private var _binding: FragmentHogwatsStaffBinding? = null
    private val binding get() = _binding!!
    @Inject
    lateinit var hpAdapter: HPAdapter
    private val staffViewModel by viewModels<StaffViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentHogwatsStaffBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initViews()
    }

    private fun initViews() = with(binding) {
        rvStaff.adapter = hpAdapter
        staffViewModel.getStaff()
    }

    private fun initObservers() = lifecycleScope.launch {
        viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
            staffViewModel.staff.collectLatest { state ->
                hpAdapter.updateCategory(state.staffList)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}