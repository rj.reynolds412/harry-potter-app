package com.example.harrypotter.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.example.harrypotter.adapter.HPAdapter
import com.example.harrypotter.databinding.FragmentRavenclawHouseBinding
import com.example.harrypotter.viewModel.RavenclawViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class RavenclawHouseFragment : Fragment() {

    private var _binding: FragmentRavenclawHouseBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var hpAdapter: HPAdapter
    private val ravenclawViewModel by viewModels<RavenclawViewModel>()
    private val args by navArgs<RavenclawHouseFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentRavenclawHouseBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initViews()
    }

    private fun initViews() = with(binding) {
        val ravenArgs = args.house
        rvRavenclaw.adapter = hpAdapter
        ravenclawViewModel.getRavenclaws(ravenArgs)
    }

    private fun initObservers() = lifecycleScope.launch {
        viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
            ravenclawViewModel.ravenclaws.collectLatest { state ->
                hpAdapter.updateCategory(state.ravenclawList)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}