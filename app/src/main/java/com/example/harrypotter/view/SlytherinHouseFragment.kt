package com.example.harrypotter.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.example.harrypotter.adapter.HPAdapter
import com.example.harrypotter.databinding.FragmentSlytherinHouseBinding
import com.example.harrypotter.viewModel.SlytherinViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SlytherinHouseFragment : Fragment() {

    private var _binding: FragmentSlytherinHouseBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var hpAdapter: HPAdapter
    private val slytherinViewModel by viewModels<SlytherinViewModel>()
    private val args by navArgs<SlytherinHouseFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentSlytherinHouseBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initViews()
    }

    private fun initViews() = with(binding) {
        val snakeArgs = args.house
        rvSlytherin.adapter = hpAdapter
        slytherinViewModel.getSlytherins(snakeArgs)
    }

    private fun initObservers() = lifecycleScope.launch {
        viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
            slytherinViewModel.slytherins.collectLatest { state ->
                hpAdapter.updateCategory(state.slytherinList)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}