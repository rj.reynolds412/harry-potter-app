package com.example.harrypotter.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.example.harrypotter.adapter.HPAdapter
import com.example.harrypotter.databinding.FragmentHufflepuffHouseBinding
import com.example.harrypotter.viewModel.HufflepuffViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class HufflepuffHouseFragment : Fragment() {


    private var _binding: FragmentHufflepuffHouseBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var hpAdapter: HPAdapter
    private val hufflepuffViewModel by viewModels<HufflepuffViewModel>()
    private val args by navArgs<HufflepuffHouseFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentHufflepuffHouseBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initView()
    }

    private fun initView() = with(binding) {
        val badgerArgs = args.house
        rvHufflepuff.adapter = hpAdapter
        hufflepuffViewModel.getHufflepuffs(badgerArgs)
    }

    private fun initObservers() = lifecycleScope.launch {
        viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
            hufflepuffViewModel.hufflepuffs.collectLatest { state ->
                hpAdapter.updateCategory(state.hufflepuffList)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}