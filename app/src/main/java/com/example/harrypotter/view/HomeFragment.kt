package com.example.harrypotter.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.harrypotter.databinding.FragmentHomeBinding
import com.example.harrypotter.utils.House
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentHomeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        btnAllCharacters.setOnClickListener {
            val allNav = HomeFragmentDirections.actionHomeFragmentToAllCharacterFragment()
            findNavController().navigate(allNav)
        }
        btnStudents.setOnClickListener {
            val studentNav = HomeFragmentDirections.actionHomeFragmentToHogwartsStudentFragment()
            findNavController().navigate(studentNav)
        }
        btnStaff.setOnClickListener {
            val staffNav = HomeFragmentDirections.actionHomeFragmentToHogwartsStaffFragment()
            findNavController().navigate(staffNav)
        }
        gryffindor.setOnClickListener {
            val lionNav =
                HomeFragmentDirections.actionHomeFragmentToGryffindorHouseFragment(house = House.GRYFFINDOR.name)
            findNavController().navigate(lionNav)
        }
        hufflepuff.setOnClickListener {
            val badgerNav =
                HomeFragmentDirections.actionHomeFragmentToHufflepuffHouseFragment(house = House.HUFFLEPUFF.name)
            findNavController().navigate(badgerNav)
        }
        ravenclaw.setOnClickListener {
            val ravenNav =
                HomeFragmentDirections.actionHomeFragmentToRavenclawHouseFragment(house = House.RAVENCLAW.name)
            findNavController().navigate(ravenNav)
        }
        slytherin.setOnClickListener {
            val snakeNav =
                HomeFragmentDirections.actionHomeFragmentToSlytherinHouseFragment(house = House.SLYTHERIN.name)
            findNavController().navigate(snakeNav)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}