package com.example.harrypotter.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.example.harrypotter.adapter.HPAdapter
import com.example.harrypotter.databinding.FragmentGryffindorHouseBinding
import com.example.harrypotter.viewModel.GryffindorViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class GryffindorHouseFragment : Fragment() {

    private var _binding: FragmentGryffindorHouseBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var hpAdapter: HPAdapter
    private val gryffindorViewModel by viewModels<GryffindorViewModel>()
    private val args by navArgs<GryffindorHouseFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentGryffindorHouseBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initViews()
    }

    private fun initViews() = with(binding) {
        val lionArgs = args.house
        rvGryffindor.adapter = hpAdapter
        gryffindorViewModel.getGryffindors(lionArgs)
    }

    private fun initObservers() = lifecycleScope.launch {
        viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
            gryffindorViewModel.gryffindors.collectLatest { state ->
                hpAdapter.updateCategory(state.gryffindorList)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}