package com.example.harrypotter.di

import android.app.Application
import androidx.room.Room
import com.example.harrypotter.adapter.HPAdapter
import com.example.harrypotter.domain.*
import com.example.harrypotter.local.dao.HPDao
import com.example.harrypotter.local.dao.entity.HPDatabase
import com.example.harrypotter.model.HPRepo
import com.example.harrypotter.model.HPService
import com.example.harrypotter.model.HPService.Companion.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object HPModule {


    @Provides
    @Singleton
    fun providesHPService(): HPService = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(HPService::class.java)

    @Provides
    @Singleton
    fun providesDatabaseInstance(app: Application) =
        Room.databaseBuilder(app, HPDatabase::class.java, "HP_DB").build()

    @Provides
    @Singleton
    fun providesHpDao(db: HPDatabase) = db.hpDao()


    @Provides
    @Singleton
    fun providesHPRepo(hpService: HPService, hpDao: HPDao)
            : HPRepo = HPRepo(hpService, hpDao)

    @Provides
    @Singleton
    fun providesHPAdapter(): HPAdapter = HPAdapter()

    @Provides
    @Singleton
    fun provideAllCharacterUseCase(hpRepo: HPRepo)
            : GetAllCharactersUseCase = GetAllCharactersUseCase(hpRepo)

    @Provides
    @Singleton
    fun providesStudentUseCase(hpRepo: HPRepo)
            : GetStudentsUseCase = GetStudentsUseCase(hpRepo)

    @Provides
    @Singleton
    fun providesStaffUseCase(hpRepo: HPRepo)
            : GetStaffUseCase = GetStaffUseCase(hpRepo)

    @Provides
    @Singleton
    fun providesGryffindorUseCase(hpRepo: HPRepo)
            : GetGryffindorUseCase = GetGryffindorUseCase(hpRepo)

    @Provides
    @Singleton
    fun providesHufflepuffUseCase(hpRepo: HPRepo)
            : GetHufflePuffUseCase = GetHufflePuffUseCase(hpRepo)

    @Provides
    @Singleton
    fun providesRavenclawUseCase(hpRepo: HPRepo)
            : GetRavenclawUseCase = GetRavenclawUseCase(hpRepo)

    @Provides
    @Singleton
    fun providesSlytherinUseCase(hpRepo: HPRepo)
            : GetSlytherinUseCase = GetSlytherinUseCase(hpRepo)
}