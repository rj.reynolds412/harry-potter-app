package com.example.harrypotter.domain

import com.example.harrypotter.local.dao.entity.Characters
import com.example.harrypotter.model.HPRepo
import com.example.harrypotter.model.response.HPResponseItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetSlytherinUseCase @Inject constructor(private val hpRepo: HPRepo) {
    suspend operator fun invoke(houseName: String): Result<List<Characters>> = withContext(
        Dispatchers.IO) {
        return@withContext try {
            val response = hpRepo.getHouseCharacters(houseName)
            val slytherinChar: List<Characters> = response
            Result.success(slytherinChar)
        } catch (ex: Exception) {
            Result.failure(ex)
        }
    }
}