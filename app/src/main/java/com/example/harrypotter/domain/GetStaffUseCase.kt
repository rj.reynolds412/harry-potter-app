package com.example.harrypotter.domain

import com.example.harrypotter.local.dao.entity.Characters
import com.example.harrypotter.model.HPRepo
import com.example.harrypotter.model.response.HPResponseItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetStaffUseCase @Inject constructor(private val hpRepo: HPRepo) {
    suspend operator fun invoke(): Result<List<Characters>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = hpRepo.getStaff()
            val staff: List<Characters> = response
            Result.success(staff)
        } catch (ex: Exception) {
            Result.failure(ex)
        }
    }
}