package com.example.harrypotter.utils

enum class House(value: String) {
    GRYFFINDOR("Gryffindor"),
    SLYTHERIN("Slytherin"),
    HUFFLEPUFF("Hufflepuff"),
    RAVENCLAW("Ravenclaw")
}
