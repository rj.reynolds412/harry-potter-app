package com.example.harrypotter.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.harrypotter.databinding.*
import com.example.harrypotter.local.dao.entity.Characters

class HPAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var characters = mutableListOf<Characters>()
    private var characterType: CharacterTypes = CharacterTypes.ALL


    class AllCharactersViewHolder(private val binding: ItemAllCharactersBinding) :
        BaseViewHolder(binding.root) {

        companion object {
            fun getAllCharactersInstance(parent: ViewGroup) = ItemAllCharactersBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { AllCharactersViewHolder(it) }
        }

        override fun displayItem(item: Characters) {
            Log.e("TAG", "displayItem: ${item.image}")
            binding.ivAllCharacters.load(item.image) {
            }
            binding.tvAllCharacterDetails.text = listOfNotNull(
                item.name,
                item.actor,
                item.gender,
                item.dateOfBirth,
                item.species,
                item.ancestry,
                item.house,
                item.patronus,
            ).joinToString("\n")
        }

    }

    class StudentsViewHolder(private val binding: ItemStudentsBinding) :
        BaseViewHolder(binding.root) {

        companion object {
            fun getStudentsInstance(parent: ViewGroup) = ItemStudentsBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { StudentsViewHolder(it) }
        }

        override fun displayItem(item: Characters) {
            binding.ivStudents.load(item.image)
            binding.tvStudentDetails.text = listOfNotNull(
                item.name,
                item.actor,
                item.gender,
                item.dateOfBirth,
                item.species,
                item.ancestry,
                item.house,
                item.patronus,
            ).joinToString("\n")
        }

    }

    class StaffViewHolder(private val binding: ItemStaffBinding) :
        BaseViewHolder(binding.root) {

        companion object {
            fun getStaffInstance(parent: ViewGroup) = ItemStaffBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { StaffViewHolder(it) }
        }

        override fun displayItem(item: Characters) {
            binding.ivStaff.load(item.image)
            binding.tvStaffDetails.text = listOfNotNull(
                item.name,
                item.actor,
                item.gender,
                item.dateOfBirth,
                item.species,
                item.ancestry,
                item.house,
                item.patronus,
            ).joinToString("\n")
        }
    }

    class GryffindorViewHolder(private val binding: ItemGryffindorBinding) :
        BaseViewHolder(binding.root) {

        companion object {
            fun getGryffindorInstance(parent: ViewGroup) = ItemGryffindorBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { GryffindorViewHolder(it) }
        }

        override fun displayItem(item: Characters) {
            binding.ivGryffindor.load(item.image)
            binding.tvGryffindorDetails.text = listOfNotNull(
                item.name,
                item.actor,
                item.gender,
                item.dateOfBirth,
                item.species,
                item.ancestry,
                item.house,
                item.patronus,
            ).joinToString("\n")
        }
    }

    class HufflepuffViewHolder(private val binding: ItemHufflepuffBinding) :
        BaseViewHolder(binding.root) {

        companion object {
            fun getHufflepuffInstance(parent: ViewGroup) = ItemHufflepuffBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { HufflepuffViewHolder(it) }
        }

        override fun displayItem(item: Characters) {
            binding.ivHufflepuff.load(item.image)
            binding.tvHufflepuffDetails.text = listOfNotNull(
                item.name,
                item.actor,
                item.gender,
                item.dateOfBirth,
                item.species,
                item.ancestry,
                item.house,
                item.patronus,
            ).joinToString("\n")
        }
    }

    class RavenclawViewHolder(private val binding: ItemRavenclawBinding) :
        BaseViewHolder(binding.root) {

        companion object {
            fun getRavenclawInstance(parent: ViewGroup) = ItemRavenclawBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { RavenclawViewHolder(it) }
        }

        override fun displayItem(item: Characters) {
            binding.ivRavenclaw.load(item.image)
            binding.tvRavenclawDetails.text = listOfNotNull(
                item.name,
                item.actor,
                item.gender,
                item.dateOfBirth,
                item.species,
                item.ancestry,
                item.house,
                item.patronus,
            ).joinToString("\n")
        }
    }

    class SlytherinViewHolder(private val binding: ItemSlytherinBinding) :
        BaseViewHolder(binding.root) {

        companion object {
            fun getSlytherinInstance(parent: ViewGroup) = ItemSlytherinBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { SlytherinViewHolder(it) }
        }

        override fun displayItem(item: Characters) {
            binding.ivSlytherin.load(item.image)
            binding.tvSlytherinDetails.text = listOfNotNull(
                item.name,
                item.actor,
                item.gender,
                item.dateOfBirth,
                item.species,
                item.ancestry,
                item.house,
                item.patronus,
            ).joinToString("\n")
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (characterType) {
            CharacterTypes.STAFF -> {
                StaffViewHolder.getStaffInstance(parent)
            }
            CharacterTypes.STUDENT -> {
                StudentsViewHolder.getStudentsInstance(parent)
            }
            CharacterTypes.ALL -> {
                AllCharactersViewHolder.getAllCharactersInstance(parent)
            }
            CharacterTypes.GRYFFINDOR -> {
                GryffindorViewHolder.getGryffindorInstance(parent)

            }
            CharacterTypes.HUFFLEPUFF -> {
                HufflepuffViewHolder.getHufflepuffInstance(parent)
            }
            CharacterTypes.RAVENCLAW -> {
                RavenclawViewHolder.getRavenclawInstance(parent)

            }
            CharacterTypes.SLYTHERIN -> {
                SlytherinViewHolder.getSlytherinInstance(parent)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as BaseViewHolder).displayItem(characters[position])
    }

    override fun getItemCount(): Int = characters.size

    fun updateCategory(category: List<Characters>) {
        this.characters.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(category)
            notifyItemRangeInserted(0, size)
        }
    }
}


abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun displayItem(item: Characters)
}

enum class CharacterTypes {
    STAFF, STUDENT, ALL, GRYFFINDOR, HUFFLEPUFF, RAVENCLAW, SLYTHERIN
}