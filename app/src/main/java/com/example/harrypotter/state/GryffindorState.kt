package com.example.harrypotter.state

import com.example.harrypotter.local.dao.entity.Characters
import com.example.harrypotter.model.response.HPResponseItem

data class GryffindorState(
    val isloading: Boolean = false,
    val gryffindorList: List<Characters> = emptyList(),
    val error: String? = null,
)
