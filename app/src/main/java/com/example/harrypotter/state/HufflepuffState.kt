package com.example.harrypotter.state

import com.example.harrypotter.local.dao.entity.Characters
import com.example.harrypotter.model.response.HPResponseItem

data class HufflepuffState(
    val isloading: Boolean = false,
    val hufflepuffList: List<Characters> = emptyList(),
    val error: String? = null,
)
