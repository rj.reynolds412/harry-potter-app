package com.example.harrypotter.state

import com.example.harrypotter.local.dao.entity.Characters
import com.example.harrypotter.model.response.HPResponseItem

data class SlytherinState(
    val isloading: Boolean = false,
    val slytherinList: List<Characters> = emptyList(),
    val error: String? = null,
)
