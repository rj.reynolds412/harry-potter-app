package com.example.harrypotter.state

import com.example.harrypotter.local.dao.entity.Characters
import com.example.harrypotter.model.response.HPResponseItem

data class StudentState(
    val isLoading: Boolean = false,
    val studentList: List<Characters> = emptyList(),
    val error: String? = null,
)
