package com.example.harrypotter.state

import com.example.harrypotter.local.dao.entity.Characters
import com.example.harrypotter.model.response.HPResponseItem

data class AllCharacterState(
    val isLoading: Boolean = false,
    val allChars: List<Characters> = listOf(),
    val error: String? = null,
)
