package com.example.harrypotter.state

import com.example.harrypotter.local.dao.entity.Characters
import com.example.harrypotter.model.response.HPResponseItem

data class StaffState(
    val isloading: Boolean = false,
    val staffList: List<Characters> = emptyList(),
    val error: String? = null,
)
