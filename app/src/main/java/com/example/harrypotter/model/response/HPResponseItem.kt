package com.example.harrypotter.model.response


import com.google.gson.annotations.SerializedName


data class HPResponseItem(
    val actor: String = "",
    val alive: Boolean = false,
    @SerializedName("alternate_actors")
    val alternateActors: List<Any> = emptyList(),
    @SerializedName("alternate_names")
    val alternateNames: List<Any> = emptyList(),
    val ancestry: String = "",
    val dateOfBirth: String = "",
    val eyeColour: String = "",
    val gender: String = "",
    val hairColour: String = "",
    val hogwartsStaff: Boolean = false,
    val hogwartsStudent: Boolean = false,
    val house: String = "",
    val image: String = "",
    val name: String = "",
    val patronus: String = "",
    val species: String = "",
    val wand: Wand = Wand(),
    val wizard: Boolean = false,
    val yearOfBirth: String = "",
)
