package com.example.harrypotter.model.response


data class Wand(
    val core: String = "",
    val length: String = "",
    val wood: String = "",
)