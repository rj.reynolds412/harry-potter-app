package com.example.harrypotter.model

import com.example.harrypotter.local.dao.HPDao
import com.example.harrypotter.local.dao.entity.Characters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HPRepo @Inject constructor(private val hpService: HPService, private val hpDao: HPDao) {

    suspend fun getAllCharacters() = withContext(Dispatchers.IO) {
        val savedCharacters: List<Characters> = hpDao.getAll()

        return@withContext savedCharacters.ifEmpty {
            val characters = hpService.getAllCharacters()
            val character: List<Characters> = characters.map {
                Characters(
                    actor = it.actor,
                    ancestry = it.ancestry,
                    dateOfBirth = it.dateOfBirth,
                    house = it.house,
                    image = it.image,
                    name = it.name,
                    patronus = it.patronus,
                    species = it.species,
                    gender = it.gender
                )
            }
            hpDao.insert(*character.toTypedArray())
            return@ifEmpty character
        }
    }

    suspend fun getStudents() = withContext(Dispatchers.IO) {
        val savedStudents: List<Characters> = hpDao.getStudents()

        return@withContext savedStudents.ifEmpty {
            val students = hpService.getStudents()
            val student: List<Characters> = students.map {
                Characters(
                    actor = it.actor,
                    ancestry = it.ancestry,
                    dateOfBirth = it.dateOfBirth,
                    house = it.house,
                    image = it.image,
                    name = it.name,
                    patronus = it.patronus,
                    species = it.species,
                    gender = it.gender
                )
            }
            hpDao.insertStudents(*student.toTypedArray())
            return@ifEmpty student
        }
    }

    suspend fun getStaff() = withContext(Dispatchers.IO) {
        val savedStaff: List<Characters> = hpDao.getStaff()

        return@withContext savedStaff.ifEmpty {
            val allStaff = hpService.getStaff()
            val staff: List<Characters> = allStaff.map {
                Characters(
                    actor = it.actor,
                    ancestry = it.ancestry,
                    dateOfBirth = it.dateOfBirth,
                    house = it.house,
                    image = it.image,
                    name = it.name,
                    patronus = it.patronus,
                    species = it.species,
                    gender = it.gender
                )
            }
            hpDao.insertStaff(*staff.toTypedArray())
            return@ifEmpty staff
        }
    }

    suspend fun getHouseCharacters(houseName: String) = withContext(Dispatchers.IO) {
        val savedHouseCharacters: List<Characters> = hpDao.getHouseCharacters(houseName)

        return@withContext savedHouseCharacters.ifEmpty {
            val houseCharacters = hpService.getHouseCharacters(houseName)
            val houseCharacter: List<Characters> = houseCharacters.map {
                Characters(
                    actor = it.actor,
                    ancestry = it.ancestry,
                    dateOfBirth = it.dateOfBirth,
                    house = it.house,
                    image = it.image,
                    name = it.name,
                    patronus = it.patronus,
                    species = it.species,
                    gender = it.gender
                )
            }
            hpDao.insertHouseCharacters(*houseCharacter.toTypedArray())
            return@ifEmpty houseCharacter
        }
    }
}