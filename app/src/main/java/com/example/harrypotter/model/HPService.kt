package com.example.harrypotter.model

import com.example.harrypotter.model.response.HPResponseItem
import retrofit2.http.GET
import retrofit2.http.Path

interface HPService {

    companion object {
        const val BASE_URL = "https://hp-api.onrender.com/"
        private const val ALL_CHARACTER_ENDPOINT = "api/characters"
        private const val STUDENT_ENDPOINT = "api/characters/students"
        private const val STAFF_ENDPOINT = "api/characters/staff"
        private const val CHARACTER_BY_HOUSE_ENDPOINT = "api/characters/house/{house_name}"

    }

    @GET(ALL_CHARACTER_ENDPOINT)
    suspend fun getAllCharacters(): List<HPResponseItem>

    @GET(STUDENT_ENDPOINT)
    suspend fun getStudents(): List<HPResponseItem>

    @GET(STAFF_ENDPOINT)
    suspend fun getStaff(): List<HPResponseItem>

    @GET(CHARACTER_BY_HOUSE_ENDPOINT)
    suspend fun getHouseCharacters(@Path("house_name") houseName: String): List<HPResponseItem>
}