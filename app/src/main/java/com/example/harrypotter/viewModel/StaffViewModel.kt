package com.example.harrypotter.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.harrypotter.domain.GetStaffUseCase
import com.example.harrypotter.state.StaffState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StaffViewModel @Inject constructor(
    private val getStaffUseCase: GetStaffUseCase,
) : ViewModel() {

    private val _staff = MutableStateFlow(StaffState(isloading = true))
    val staff = _staff.asStateFlow()

    fun getStaff() {
        viewModelScope.launch {
            val result = getStaffUseCase.invoke()
            if (result.isSuccess) {
                val staff = result.getOrDefault(listOf())
                _staff.value = StaffState(
                    isloading = false,
                    staffList = staff
                )
            } else {
                _staff.value = StaffState(
                    isloading = false,
                    error = result.exceptionOrNull()?.message
                )
            }
        }
    }
}