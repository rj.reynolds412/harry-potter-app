package com.example.harrypotter.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.harrypotter.domain.GetStudentsUseCase
import com.example.harrypotter.state.StudentState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StudentViewModel @Inject constructor(
    private val getStudentsUseCase: GetStudentsUseCase,
) : ViewModel() {

    private val _students = MutableStateFlow(StudentState(isLoading = true))
    val students = _students.asStateFlow()

    fun getStudents() {
        viewModelScope.launch {
            val result = getStudentsUseCase.invoke()
            if (result.isSuccess) {
                val students = result.getOrDefault(listOf())
                _students.value = StudentState(
                    isLoading = false,
                    studentList = students
                )
            } else {
                _students.value = StudentState(
                    isLoading = false,
                    error = result.exceptionOrNull()?.message
                )
            }
        }
    }
}