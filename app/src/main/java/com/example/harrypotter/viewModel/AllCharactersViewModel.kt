package com.example.harrypotter.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.harrypotter.domain.GetAllCharactersUseCase
import com.example.harrypotter.state.AllCharacterState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AllCharactersViewModel @Inject constructor(
    private val getAllCharactersUseCase: GetAllCharactersUseCase,
) : ViewModel() {

    private val _allCharacters = MutableStateFlow(AllCharacterState(isLoading = true))
    val allCharacters = _allCharacters.asStateFlow()

    fun getAllCharacters() {
        viewModelScope.launch {
            val result = getAllCharactersUseCase.invoke()
            if (result.isSuccess) {
                val characters = result.getOrDefault(listOf())
                _allCharacters.value = AllCharacterState(
                    isLoading = false,
                    allChars = characters
                )
            } else {
                _allCharacters.value = AllCharacterState(
                    isLoading = false,
                    error = result.exceptionOrNull()?.message
                )
            }
        }
    }
}