package com.example.harrypotter.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.harrypotter.domain.GetSlytherinUseCase
import com.example.harrypotter.state.SlytherinState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SlytherinViewModel @Inject constructor(
    private val getSlytherinUseCase: GetSlytherinUseCase,
) : ViewModel() {

    private val _slytherins = MutableStateFlow(SlytherinState(isloading = true))
    val slytherins = _slytherins.asStateFlow()

    fun getSlytherins(houseName: String) {
        viewModelScope.launch {
            val result = getSlytherinUseCase.invoke(houseName)
            if (result.isSuccess) {
                val gryffindors = result.getOrDefault(listOf())
                _slytherins.value = SlytherinState(
                    isloading = false,
                    slytherinList = gryffindors
                )
            } else {
                _slytherins.value = SlytherinState(
                    isloading = false,
                    error = result.exceptionOrNull()?.message
                )
            }
        }
    }
}