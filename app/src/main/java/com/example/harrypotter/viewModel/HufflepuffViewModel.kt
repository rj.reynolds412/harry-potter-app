package com.example.harrypotter.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.harrypotter.domain.GetHufflePuffUseCase
import com.example.harrypotter.state.HufflepuffState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HufflepuffViewModel @Inject constructor(
    private val getHufflePuffUseCase: GetHufflePuffUseCase,
) : ViewModel() {

    private val _huflepuffs = MutableStateFlow(HufflepuffState(isloading = true))
    val hufflepuffs = _huflepuffs.asStateFlow()

    fun getHufflepuffs(houseName: String) {
        viewModelScope.launch {
            val result = getHufflePuffUseCase.invoke(houseName)
            if (result.isSuccess) {
                val hufflepuffs = result.getOrDefault(listOf())
                _huflepuffs.value = HufflepuffState(
                    isloading = false,
                    hufflepuffList = hufflepuffs
                )
            } else {
                _huflepuffs.value = HufflepuffState(
                    isloading = false,
                    error = result.exceptionOrNull()?.message
                )
            }
        }
    }
}