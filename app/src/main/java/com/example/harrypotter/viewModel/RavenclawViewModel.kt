package com.example.harrypotter.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.harrypotter.domain.GetRavenclawUseCase
import com.example.harrypotter.state.RavenclawState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RavenclawViewModel @Inject constructor(
    private val getRavenclawUseCase: GetRavenclawUseCase,
) : ViewModel() {

    private val _ravenclaws = MutableStateFlow(RavenclawState(isloading = true))
    val ravenclaws = _ravenclaws.asStateFlow()

    fun getRavenclaws(houseName: String) {
        viewModelScope.launch {
            val result = getRavenclawUseCase.invoke(houseName)
            if (result.isSuccess) {
                val ravenclaws = result.getOrDefault(listOf())
                _ravenclaws.value = RavenclawState(
                    isloading = false,
                    ravenclawList = ravenclaws
                )
            } else {
                _ravenclaws.value = RavenclawState(
                    isloading = false,
                    error = result.exceptionOrNull()?.message
                )
            }
        }
    }
}