package com.example.harrypotter.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.harrypotter.domain.GetGryffindorUseCase
import com.example.harrypotter.state.GryffindorState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GryffindorViewModel @Inject constructor(
    private val getGryffindorUseCase: GetGryffindorUseCase,
) : ViewModel() {

    private val _gryffindors = MutableStateFlow(GryffindorState(isloading = true))
    val gryffindors = _gryffindors.asStateFlow()

    fun getGryffindors(houseName: String) {
        viewModelScope.launch {
            val result = getGryffindorUseCase.invoke(houseName)
            if (result.isSuccess) {
                val gryffindors = result.getOrDefault(listOf())
                _gryffindors.value = GryffindorState(
                    isloading = false,
                    gryffindorList = gryffindors
                )
            } else {
                _gryffindors.value = GryffindorState(
                    isloading = false,
                    error = result.exceptionOrNull()?.message
                )
            }
        }
    }
}