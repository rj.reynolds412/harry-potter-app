package com.example.harrypotter.local.dao

import androidx.room.*
import com.example.harrypotter.local.dao.entity.Characters

@Dao
interface HPDao {
    @Query("SELECT * FROM character_info")
    suspend fun getAll(): List<Characters>

    @Query("SELECT * FROM character_info WHERE hogwartsStudent = 1")
    suspend fun getStudents(): List<Characters>

    @Query("SELECT * FROM character_info WHERE hogwartsStaff = 1")
    suspend fun getStaff(): List<Characters>

    @Query("SELECT * FROM character_info WHERE house is :houseName")
    suspend fun getHouseCharacters(houseName : String) : List<Characters>

    @Insert(entity = Characters::class, onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertHouseCharacters(vararg houseCharacter:Characters)

    @Insert(entity = Characters::class, onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertStudents(vararg student: Characters)

    @Insert(entity = Characters::class, onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertStaff(vararg staff: Characters)

    @Insert(entity = Characters::class, onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(vararg characters: Characters)
}