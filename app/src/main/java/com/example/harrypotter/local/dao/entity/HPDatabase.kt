package com.example.harrypotter.local.dao.entity

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.harrypotter.local.dao.HPDao

@Database(entities = [Characters::class], exportSchema = false, version = 1)
abstract class HPDatabase: RoomDatabase() {

    abstract fun hpDao(): HPDao
}