package com.example.harrypotter.local.dao.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.harrypotter.model.response.Wand
import com.google.gson.annotations.SerializedName

@Entity(tableName = "character_info")
data class Characters(
    @PrimaryKey(autoGenerate = false)
    val actor: String = "",
    val ancestry: String = "",
    val dateOfBirth: String = "",
    val house: String = "",
    val image: String = "",
    val name: String = "",
    val patronus: String = "",
    val species: String = "",
    val gender: String = "",
    val hogwartsStaff: Boolean = false,
    val hogwartsStudent: Boolean = false,
)